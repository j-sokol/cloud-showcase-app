import os
import logging
from flask import jsonify
from logging.handlers import RotatingFileHandler
from flask import Flask, request
from flask_restful import Api, abort
from cloudshowcase.resource.all_users import AllUsers
from cloudshowcase.resource.health_resource import HealthResource


class CustomApi(Api):

    def handle_error(self, e):
        return e.json()


def create_app():

    app = Flask(__name__)

    api = CustomApi(app)

    app.config['DEBUG'] = True


    # ---REGISTER RESOURCE----
    api.add_resource(AllUsers, '/users')
    api.add_resource(HealthResource, '/healthz')

    return app


# Gunicorn setup before init
if __name__ != '__main__':

    app = create_app()

    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


# Classic setup before init
if __name__ == '__main__':

    app = create_app()


    port = os.getenv("PORT", "5049")
    app.run(host='0.0.0.0', port=port)


