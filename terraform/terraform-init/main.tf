module "terraform-init" {
  source                         = "../modules/terraform-init"

  AWS_REGION                     = "${var.AWS_REGION}"
  OWNER                          = "${var.OWNER}"
  AWS_PROVIDER_ASSUME_ROLE       = "${var.AWS_PROVIDER_ASSUME_ROLE}"
  TAG_PRODUCT                               = "${var.TAG_PRODUCT}"
  TAG_APPLICATION                           = "${var.TAG_APPLICATION}"
  TAG_PROJECT                               = "${var.TAG_PROJECT}"
  TAG_CRITICALITY                           = "${var.TAG_CRITICALITY}"
  TAG_ENVIRONMENT                           = "${var.TAG_ENVIRONMENT}"
  ENVIRONMENT                = "${var.ENVIRONMENT}"
  DEPLOY_NAME                    = "${var.DEPLOY_NAME}"
  PRODUCT_NAME                   = "${var.PRODUCT_NAME}"
  KMS_KEY_ID                     = "${var.KMS_KEY_ID}"
}