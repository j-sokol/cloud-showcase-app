#################################
#
#  ZONES
#
#################################

AWS_REGION = "eu-central-1"

#################################
#
#  PERMISSIONS
#
#################################

OWNER = "dummy"

#################################
#
#  OTHER
#
#################################

TAG_PRODUCT = "Test" 
TAG_APPLICATION = "cloud-showcase"
TAG_PROJECT = "showcase"
TAG_CRITICALITY = "Critical"
TAG_ENVIRONMENT = "Dev"
ENVIRONMENT = "dev"
DEPLOY_NAME = ""
PRODUCT_NAME = "test"