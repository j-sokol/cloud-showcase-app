# variables.tf


# Global 
variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "eu-central-1"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

variable "name" {
  description = "Deployment name"
  default     = "cloud-showcase"
}

variable "environment" {
  description = "Environment name"
  default     = "prod"
}

# Container

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "registry.gitlab.com/j-sokol/cloud-showcase-app:latest"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 5049
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 3
}

variable "health_check_path" {
  default = "/healthz"
}

# ECS / Fargate

variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "myEcsTaskExecutionRole"
}


variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}



# DocumentDB

variable "docdb_instance_class" {
  description = "DocumentDB instance size"
  default = "db.r5.large"
}

variable "docdb_username" {
  description = "DocumentDB username"
  default = "mongo_rw"
}


variable "docdb_port" {
  description = "DocumentDB port"
  default = 27017
}


# API Gateway

variable "stage_name" {
  type        = string
  description = "API Gateway stage stub"
}

variable "cognito_arn" {
  description = "ARN of cognito resource"
}


variable "authorization_scopes_get" {
  description = "Cognito pool authorization scope"
}
# variable "authorization_scopes_post" {
#   description = "Cognito pool authorization scope"
# }

variable "api_endpoint" {
  description = "API endpoint stub"
}



# DNS

variable "zone" {
  type        = string
  description = "The Route53 zone in which to add the DNS entry"
}

variable "domain" {
  type        = string
  description = "The domain name for your API Gateway endpoint"
}

# NLB

variable "health_check_interval" {
  default = "30"
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused"
  default = "30"
}

variable "lb_protocol" {
  default = "TCP"
}
