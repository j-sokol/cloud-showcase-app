resource "aws_api_gateway_authorizer" "main" {
    name = "${var.name}-rest-api"
    type = "COGNITO_USER_POOLS" 
    rest_api_id = aws_api_gateway_rest_api.main.id
    provider_arns = ["${var.cognito_arn}"]
}

