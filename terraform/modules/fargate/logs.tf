# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
# Fargate logs
resource "aws_cloudwatch_log_group" "cb_log_group" {
  name              = "/ecs/${var.name}-app"
  retention_in_days = 30

}

resource "aws_cloudwatch_log_stream" "cb_log_stream" {
  name           = "${var.name}-log-stream"
  log_group_name = aws_cloudwatch_log_group.cb_log_group.name
}
