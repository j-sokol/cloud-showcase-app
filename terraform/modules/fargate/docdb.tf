resource "aws_docdb_subnet_group" "docdb" {
  name       = "${var.name}-docdb"
  subnet_ids = aws_subnet.private.*.id
}

resource "aws_docdb_cluster_instance" "docdb" {
  count              = 1
  identifier         = "${var.name}-${count.index}"
  cluster_identifier = aws_docdb_cluster.docdb.id
  instance_class     = var.docdb_instance_class
}

resource "aws_docdb_cluster" "docdb" {
  skip_final_snapshot     = true
  db_subnet_group_name    = aws_docdb_subnet_group.docdb.name
  cluster_identifier      = "${var.name}-docdb"
  engine                  = "docdb"
  master_username         = var.docdb_username
  master_password         = random_string.docdb_database_password.result
  
  db_cluster_parameter_group_name = aws_docdb_cluster_parameter_group.docdb.name
  vpc_security_group_ids = ["${aws_security_group.docdb.id}"]
}

resource "aws_docdb_cluster_parameter_group" "docdb" {
  family = "docdb3.6"
  name = "tf-${var.name}"

  parameter {
    name  = "tls"
    value = "disabled"
  }
}

resource "aws_ssm_parameter" "docdb_password" {
  name        = "/${var.environment}/mongo/password/master"
  description = "The parameter description"
  type        = "SecureString"
  value       = random_string.docdb_database_password.result


#   tags = {
#     environment = "${var.environment}"
#   }
}

resource "random_string" "docdb_database_password" {
  length           = 16
  special          = false
}
