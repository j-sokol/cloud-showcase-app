# ecs.tf

resource "aws_ecs_cluster" "main" {
  name = "${var.name}-ecs-cluster"
}

data "template_file" "cb_app" {
  template = file("./templates/ecs/cb_app.json.tpl")

  vars = {
    app_image      = var.app_image
    app_port       = var.app_port
    name           = "${var.name}-app"

    fargate_cpu    = var.fargate_cpu
    fargate_memory = var.fargate_memory
    aws_region     = var.aws_region
    docdb_password_secret_arn = aws_ssm_parameter.docdb_password.arn
    docdb_endpoint = aws_docdb_cluster.docdb.endpoint
    docdb_username = var.docdb_username
    docdb_password = random_string.docdb_database_password.result

    # app_image      = var.app_image
    # name           = "${var.name}-app"
    # app_port       = var.app_port
    # fargate_cpu    = var.fargate_cpu
    # fargate_memory = var.fargate_memory
    # aws_region     = var.aws_region
    # # docdb_password_secret_arn = "test"
    # docdb_endpoint = "test"
    # docdb_username = "test"
    # docdb_password = "test"

  }
}

resource "aws_ecs_task_definition" "app" {
  family                   = "${var.name}-app-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.cb_app.rendered
}

resource "aws_ecs_service" "main" {
  name            = "${var.name}-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.main.id
    container_name   = "${var.name}-app"
    container_port   = var.app_port
  }

  depends_on = [aws_lb_listener.tcp, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

# data "aws_ami" "ubuntu" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["099720109477"] # Canonical
# }

# resource "aws_instance" "web" {
#   ami           = "${data.aws_ami.ubuntu.id}"
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [aws_security_group.ecs_tasks.id]
#   subnet_id              = aws_subnet.public.0.id
#   key_name  = "jansokol@jsokol-mac2.local"
# }

