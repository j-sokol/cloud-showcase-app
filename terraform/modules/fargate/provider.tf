# provider.tf

# Specify the provider and access details
provider "aws" {
  # shared_credentials_file = "$HOME/.aws/credentials"
  profile                 = "default"
  region                  = var.aws_region
}

provider "random" {
  version = "~> 2.1"
}
