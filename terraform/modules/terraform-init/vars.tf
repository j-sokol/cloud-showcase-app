#################################
#
#  ZONES
#
#################################

variable "AWS_REGION" {
  type = "string"
}

variable "KMS_KEY_ID" {
  type = "string"
}

#################################
#
#  PERMISSIONS
#
#################################

variable "OWNER" {
  type = "string"
}

variable "AWS_PROVIDER_ASSUME_ROLE" {
  type = "string"
}

#################################
#
#  OTHER
#
#################################

variable "TAG_PRODUCT" {
  type = "string" 
}

variable "TAG_APPLICATION" {
  type = "string"
}

variable "TAG_PROJECT" {
  type = "string"
}

variable "TAG_CRITICALITY" {
  type = "string"
}

variable "TAG_ENVIRONMENT" {
  type = "string"
}

variable "ENVIRONMENT" {
  type = "string"
}

variable "DEPLOY_NAME" {
  type = "string"
}

variable "PRODUCT_NAME" {
  type = "string"
}