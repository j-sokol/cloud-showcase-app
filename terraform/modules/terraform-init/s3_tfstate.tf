resource "aws_s3_bucket" "terraform_s3_tfstate_bucket" {
  bucket = "s3-${var.PRODUCT_NAME}-${var.ENVIRONMENT}-tfstate${var.DEPLOY_NAME}"
  acl    = "private"

  versioning {
    enabled = true
  }


  tags = {
    Name = "s3-${var.PRODUCT_NAME}-${var.ENVIRONMENT}-tfstate${var.DEPLOY_NAME}"
    Owner = "${var.OWNER}"
    Creator = "${var.OWNER}"
    Product    = "${var.TAG_PRODUCT}"
    Application   = "${var.TAG_APPLICATION}"
    Project       = "${var.TAG_PROJECT}"
    Criticality   = "${var.TAG_CRITICALITY}"
    Environment   = "${var.TAG_ENVIRONMENT}"
  }
}