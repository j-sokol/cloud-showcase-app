# API Gateway
domain = "api.creativewaste.club"
stage_name = "v1"
zone = "creativewaste.club"


# Fargate
app_port = 5049
az_count = 2
app_count = 2
health_check_path = "/healthz"
fargate_cpu = "256"
fargate_memory = "512"

# General 
app_image = "registry.gitlab.com/j-sokol/cloud-showcase-app:latest"
aws_region = "eu-central-1"
name = "cloud-showcase"
api_endpoint = "users"
