[
  {
    "name": "${name}",
    "image": "${app_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/${name}",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      }
    ],
    "environment" : [
      { 
        "name" : "MONGO_HOSTNAME", 
        "value" : "${docdb_endpoint}" 
      },
      {     
        "name" : "MONGO_USERNAME", 
        "value" : "${docdb_username}" 
      }

    ],
    "secrets": [
      {
          "name": "MONGO_PASSWORD",
          "valueFrom": "${docdb_password_secret_arn}"
      }
    ]

  }
]
