
module "fargate" {
  source = "../modules/fargate"

  # General
  name = var.name
  aws_region = "eu-central-1"
  app_image = var.app_image

  # Fargate
  app_port = var.app_port
  az_count = var.az_count
  app_count = var.app_count
  health_check_path = var.health_check_path
  fargate_cpu = var.fargate_cpu
  fargate_memory = var.fargate_memory

  # API Gateway
  zone = var.zone
  domain = var.domain
  stage_name = var.stage_name
  api_endpoint = var.api_endpoint

  # API Gateway AUTH
  cognito_arn = "${module.cognito.arn}"
  authorization_scopes_get = [ "https://${var.domain}/${var.api_endpoint}"]
}

module "cognito" {

  source = "../modules/cognito"
  user_pool_name = "${var.name}-pool"

  # App client
  client_name                                 = "client0"
  client_allowed_oauth_flows_user_pool_client = true
  client_callback_urls                        = ["https://${var.domain}"]
  client_default_redirect_uri                 = "https://${var.domain}"
  client_read_attributes                      = ["email"]
  client_refresh_token_validity               = 30
  client_allowed_oauth_scopes = [ "https://${var.domain}/${var.api_endpoint}"]
  client_allowed_oauth_flows = ["client_credentials"]
  client_explicit_auth_flows = ["ALLOW_REFRESH_TOKEN_AUTH"]
  client_supported_identity_providers = [ "COGNITO" ]
  admin_create_user_config_allow_admin_create_user_only = false
  
  # user_group
  user_group_name        = "mygroup"
  user_group_description = "My group"

  # Cognito hostname
  domain = var.name


  # Allowed scopes
  resource_servers = [
    {
      identifier = "https://${var.domain}"
      name       = "post-users"
      scope = [
        {
          scope_name        = "${var.api_endpoint}"
          scope_description = "Post new users"
        }
      ]
    }
  ]


}