variable "domain" {
}


variable "stage_name" {
}


variable "zone" {
}


variable "app_count" {
  
}

variable "az_count" {
  
}


variable "app_port" {
  
}

variable "health_check_path" {
  
}


variable "fargate_cpu" {
  
}


variable "fargate_memory" {
  
}


variable "app_image" {
  
}


variable "aws_region" {
  
}

variable "name" {
  
}

variable "api_endpoint" {
  
}
