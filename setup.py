from setuptools import setup, find_packages


with open('README.md') as f:
    long_description = ''.join(f.readlines())


setup(
    name='cloudshowcase',
    version='0.1',
    description='Service to showcase capabilities of AWS cloud',
    long_description=long_description,
    author='Jan Sokol',
    author_email='jan.sokol.glx@gmail.com',
    keywords='cloud,showcase',
    packages=find_packages(),
    classifiers=[
        'Intended Audience :: Developers',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'betamax'],

    zip_safe=False,
)