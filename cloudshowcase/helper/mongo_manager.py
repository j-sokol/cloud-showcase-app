from pymongo import MongoClient

from cloudshowcase.helper.config import Config


class MongoManger(object):
    """
    MongoDB Manager using Context
    """

    def __init__(self, config: Config, client=None):

        self.host = config.mongo_hostname()
        self.port = config.mongo_port()
        self.username = config.mongo_username()
        self.password = config.mongo_password()
        self.client = client

    def __enter__(self):
        if self.client:
            return self

        self.client = MongoClient(self.host, self.port, username=self.username, password=self.password)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.client.close()

    def get_users(self):
        return self.client["users"].users

