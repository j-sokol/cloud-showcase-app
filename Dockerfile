FROM python:3.7
MAINTAINER Jan Sokol "jan.sokol.glx@gmail.com"


RUN apt-get -y update
RUN apt-get install -y build-essential cmake python3 libgeos-dev

# Copy app to remote container
COPY . /app

# Set working directory
WORKDIR /app


RUN python3 -m pip install -r requirements.txt


RUN mkdir /var/log/cloudshowcase

EXPOSE 5049

ENV PYTHONPATH=/app

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
