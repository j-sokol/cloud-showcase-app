# Cloud Showcase - app

Small Python service to showcase capabilities of AWS cloud. 


Service has endpoint `/users` with `POST` and `GET` functionalities.

- `POST` adds an user to postgresql database.

An example when run locally:
```
curl -d '' -H "Content-Type: application/json" -X POST  localhost:5049/users
```
- `GET` will fetch all the users from the postgresql database.

An example:

```
curl localhost:5049/users
```

### Building up Docker container locally

To start up container with showcase app you need to have docker and docker-compose installed.

To run it in detached mode, write
```
docker-compose up -d
```
This will also create mongodb container running on hostname `mongo`, both in `cloud-showcase-network` virtual docker network.

For running python app in the container `gunicorn` is used. ([https://gunicorn.org](https://gunicorn.org)) By default gunicorn uses `/tmp` as temporary storage, but docker containers don't have `tmpfs` in that mountpoint, so this is changed to `/dev/shm`. By default gunicorn creates 3 workers, which then operate API requests.


As container basic `python:3.7` (debian) is used. To limit size of the container alpine linux could be used, or also multistage builds.

In the end destroy it with 
```
docker-compose down
```

## CI / CD

For CI / CD we use the one provided from Gitlab, and it has multiple phases. 

CI could be done differently - one option could be either hosting Gitlab CI workers on your own - In k8s, fargate, etc.
Or doing the whole CI self-hosted (such as Jenkins, or Jenkins X, the Kubernetes-native version). This option would be rather time-consuming to set up and afterwards to tune up, so I skipped this

To limit the scope, we will only build the containers (no unit tests before), then push it to the registry. Here we use Gitlab registry, but others could be used as well - such as `ECR` from `AWS`, etc.

If the CI job is run from master branch, then `latest` tag as added to the container in `release` phase.

Gitlab registry token needs to be added to CI env variables. 
Get token from [https://gitlab.com/profile/personal_access_tokens](https://gitlab.com/profile/personal_access_tokens), and enter it to ENV_VAR `GL_TOKEN`.



### Running develop version of service

Create your own virtual environment with python version `3.7`. 

```python3.7 -m venv __venv__```

Activate the virtual environment that you created

```. __venv__/bin/activate```

Install all the dependencies that routing engine requires

```pip install -r requirements.txt```


Mongodb needs to be installed. In macOS this can be done for example with brew
```
brew install mongodb-community


To have launchd start mongo now and restart at login:
  brew services start mongodb-community
```

or with docker. Mongo hostname then should be in env variable `MONGO_HOSTNAME` (default `localhost`) and it's port should be in `MONGO_PORT` (default ``).

```
python setup.py install
```
Run the app with debug server,

```
python manage.py
```

#### Notes

For this showcase, unit test phase will be out of scope.

# Cloud Showcase Environment

Repository containing environment for [Cloud Showcase App](https://gitlab.com/j-sokol/cloud-showcase-app).

There are two ways main ways how to approach this task. One is more cloud-agnostic, with Kubernetes. This way lifting the whole environment and moving it to another cloud provider is way easier - there are just few cloud specific tasks, such rewriting Kubernetes cluster part in Terraform and having network specific things in mind. Still, there might be some disadvantages - such as having to manage all the services my yourself, thus needing to have operations.

The other one is more bounded to AWS eco-system and is using their managed services. Advantages of this approach is that most of administration is already taken care of, updates are usually automatic and there could be visible savings on operations manpower. Disadvantage is being dependant on Amazon - if cloud provider raises their prices or change specification (for example diverging from RFC standards, or not being compatible with previously forked OSS version), you need to follow them, or with high effort rewrite IaaC to different provider (and also to know specifics of each cloud platform), test it again, etc.

For this task I selected the former option.

## AWS managed services approach

For this, we first need to create API tokens. Create AWS user, add it respective permissions and save it's Secret and Access tokens to env variables:
```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Terraform state is possible to store locally or remotely - we will store it in S3 bucket.

This S3 bucket is created in `terraform-init` module.


First create the bucket using
```
make init env=dev-variables
make apply env=dev-variables
```

Later on the `cloud-showcase` module uses this bucket, where it stores it's state.


### Environment description

Terraform module is separated into multiple files, each handling aprox. one topic of the whole environment.

- `network` - Creates VPC, and inside `az_count` of private and public subnets, each in one AZ. Also builds a Internet Gateway for the public subnet and NAT gateway with an Elastic IP for each private subnet to get internet connectivity.
- `roles` - Creates ECS task execution role and its policies.
- `security` - There are 3 security groups created, each for load balancer, ECS, and documentDB. Ports are open between them respectively.
- `ecs` - Container definition is created out of it's template. Databases's hostname and username are sent out in environment variables, database password is stored in SSM secret.
- `docdb` - Instead of mongodb a managed DocumentDB (with mongo compatibility) is used. 
- `nlb` - In between the VPC and the public API gateway is Network Load Balancer and VPC link. NLB Balances in TCP mode.
- `api_gateway` - deploys an
- `autoscaling` - Fargate containers are by default in 2 AZs, in 2 mandatory replicas. When CPU utilization reaches 80%, then replicas get upscaled, up to maximum count of 6. When CPU utilization lowers under 20% again, replicas got downscaled.
- `logs` - handles logs from Fargate containers and stores them in cloudwatch. There is also posibility of storing access logs from API gateway in CloudWatch - but since app already prints out those requests, I omited that.
- `dns` - creates an `CNAME` record for the regional API Gateway endpoint.
- `cert` - issues an certificate for the dns record above.

Deployment uses `Cognito User Pool` terraform module ([link](https://github.com/lgallard/terraform-aws-cognito-user-pool)) with little enhancements, on which `api_gateway` is dependant on. 

Example usage might look acordingly. Fill up variables in `vars.tfvars` in `dev-variables` directory, or create a new dir. This way you can have multiple environments deployed at the same time. Enter there all important variables.

```
# API Gateway
domain = "api.creativewaste.club"
stage_name = "v1"
zone = "creativewaste.club"


# Fargate
app_port = 5049
az_count = 2
app_count = 2
health_check_path = "/healthz"
fargate_cpu = "256"
fargate_memory = "512"

# General 
app_image = "registry.gitlab.com/j-sokol/cloud-showcase-app:latest"
aws_region = "eu-central-1"
name = "cloud-showcase"
api_endpoint = "users"
```


Environment can be created with
```
make apply env=dev-variables
```
This will also store the `.tfstate` file to remote s3 bucket.


### Issuing a token for API Gateway

After running former Terraform modules, infrastrucure applies and terraform outputs `module.cognito.client_ids`, containing `id` and also `client_secret`. Other way to read them is via terraform show, for example
```
terraform show|grep client_secret
```
With both client_id and secret key it's posible to issue a Bearer token. 


```
Replace app client id and secret accordingly. Also, the URL will change if you had selected a different domain name
curl -X POST --user <app client id>:<app client secret> 'https://cloud-showcase.auth.eu-central-1.amazoncognito.com/oauth2/token?grant_type=client_credentials' -H 'Content-Type: application/x-www-form-urlencoded'
{"access_token":"token","expires_in":3600,"token_type":"Bearer"}
```
You can use this token already to authenticate to the API endpoints.

Token can be used in request with header `Authorization: <token>`, on the `/users` endpoint. Examples are the same as above.
```
curl https://url/v1/users -H "Authorization: <token>"
```

<!-- Other way would be creating users by themselves - cognito has options to create (or approve)
This ID can be used to create an user account, to later issue tokens. 

User can register into
```
https://${hostname}.auth.eu-central-1.amazoncognito.com/login?response_type=token&client_id=${client_id}&redirect_uri=https%3A%2F%2Fgoogle.com
```
After which he has to be enabled by the administrator. Other options could be to validate himself by email, SMS, or just let the admin to create users for others.

After user is validated, he can login again and token will be issued. -->

### Destroying the environment again


Environment can be again destroyed with 
```
make apply env=dev-variables
```


### Deployment via Kubernetes


As doing another method of deployment would be time consuming, here is just an idea how to approach it.

Deployment of EKS or GKE would be selected (to simplify operations tasks with k8s cluster, such as updating every 6 months). 



GitLab registry credentials would be added to be able to download containers from remote private registries. 

Monitoring: would be handled with `prometheus-operator` project, which has it's own official helm chart, that is well customizable - from adding grafana plugins or dashboards to adding prometheus alert rules. Monitoring would be run in it's own namespace. For API monitorings `prometheus-blackbox-exporter` could be added. Later on prometheus-alerter rules to monitor added. 

Templating: Helm chart for the `cloud-showcase` app would be prepared most probably with the same sizings as in the AWS-managed setup. App having CRDs, such as `ServiceMonitor` to register into prometheus operator.


CI/CD: Deploy user would be created, and then it's credentials would be added to the Gitlab CI. With this it would be posible to deploy new versions with combination of helm and CI.

API Gateway: As a API frontend traefik would be chosen. App is registered to the traefik via CRD (`IngressRoute`). This allows for more configuration than typical Ingress definition. By default traefik only allows basic and digest auth - for identity management external service needs to be added (there called ExternalAuth), that can be any oauth2 based service. Traefik also provides cert creation via Let's Encrypt out of the box.

Database: MongoDB deployed from official chart. Although some changes would need to be made to make it more stable, this chart already provides monitoring into prometheus operator.